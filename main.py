# Importar metodos de un archivo
# from "nombre_archivo" import "nombreMetodo", para importar varios metodos debemos 
# separarlos por coma.
from clases.menu_agenda import MenuAgenda
from clases.agenda_telefonica import AgendaTelefonica

# Instanciamos a la clases MenuAgenda y  AgendaTelefonica
menuAgenda = MenuAgenda()
agendaTelefonica = AgendaTelefonica()

# optionMenu = menuAgenda.mostrar()
# print(f'Estamos recibiendo la opcion {optionMenu}')

resultado = agendaTelefonica.buscarContacto()
print(resultado)

print('------')
agendaTelefonica.numeroTelefono()


